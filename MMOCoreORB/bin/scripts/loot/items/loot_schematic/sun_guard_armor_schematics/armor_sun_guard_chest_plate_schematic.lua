armor_sun_guard_chest_plate_schematic = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/loot_schematic/armorsmith/armor/sun_guard/chest_schematic.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("armor_sun_guard_chest_plate_schematic", armor_sun_guard_chest_plate_schematic)
