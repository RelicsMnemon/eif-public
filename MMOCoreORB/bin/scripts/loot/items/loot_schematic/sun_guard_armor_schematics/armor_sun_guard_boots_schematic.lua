armor_sun_guard_boots_schematic = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/loot_schematic/armorsmith/armor/sun_guard/boots_schematic.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("armor_sun_guard_boots_schematic", armor_sun_guard_boots_schematic)
