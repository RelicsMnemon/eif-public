armor_sun_guard_cape_schematic = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/loot_schematic/armorsmith/armor/sun_guard/cape_schematic.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("armor_sun_guard_cape_schematic", armor_sun_guard_cape_schematic)
