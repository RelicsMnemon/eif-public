armor_sun_guard_belt_schematic = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/loot_schematic/armorsmith/armor/sun_guard/belt_schematic.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("armor_sun_guard_belt_schematic", armor_sun_guard_belt_schematic)
