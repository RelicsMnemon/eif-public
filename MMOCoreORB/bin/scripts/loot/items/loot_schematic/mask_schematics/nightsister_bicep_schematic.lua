nightsister_bicep_schematic = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/loot_schematic/armorsmith/armor/neutral_special/nightsister_bicep_schematic.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("nightsister_bicep_schematic", nightsister_bicep_schematic)
