painting_nightsister_lightwhip = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_nightsister_lightwhip.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_nightsister_lightwhip", painting_nightsister_lightwhip)
