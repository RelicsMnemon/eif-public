painting_tie_pilot_helmet = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_tie_pilot_helmet.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_tie_pilot_helmet", painting_tie_pilot_helmet)
