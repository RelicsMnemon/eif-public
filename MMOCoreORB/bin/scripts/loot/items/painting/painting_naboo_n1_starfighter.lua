painting_naboo_n1_starfighter = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_naboo_n1_starfighter.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_naboo_n1_starfighter", painting_naboo_n1_starfighter)
