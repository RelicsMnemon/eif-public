painting_micro_grenade_launcher = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_micro_grenade_launcher.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_micro_grenade_launcher", painting_micro_grenade_launcher)
