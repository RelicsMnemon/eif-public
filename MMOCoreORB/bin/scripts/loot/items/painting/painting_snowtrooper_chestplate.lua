painting_snowtrooper_chestplate = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_snowtrooper_chestplate.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_snowtrooper_chestplate", painting_snowtrooper_chestplate)
