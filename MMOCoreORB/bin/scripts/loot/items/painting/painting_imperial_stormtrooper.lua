painting_imperial_stormtrooper = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_imperial_stormtrooper.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_imperial_stormtrooper", painting_imperial_stormtrooper)
