painting_old_republic_lightsaber = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_old_republic_lightsaber.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_old_republic_lightsaber", painting_old_republic_lightsaber)
