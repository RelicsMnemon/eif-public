painting_nightsister_energy_lance = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_nightsister_energy_lance.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_nightsister_energy_lance", painting_nightsister_energy_lance)
