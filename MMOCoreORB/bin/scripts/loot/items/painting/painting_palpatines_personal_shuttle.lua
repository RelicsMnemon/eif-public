painting_palpatines_personal_shuttle = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_palpatines_personal_shuttle.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_palpatines_personal_shuttle", painting_palpatines_personal_shuttle)
