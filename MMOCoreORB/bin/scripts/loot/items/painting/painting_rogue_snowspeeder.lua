painting_rogue_snowspeeder = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_rogue_snowspeeder.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_rogue_snowspeeder", painting_rogue_snowspeeder)
