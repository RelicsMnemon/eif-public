painting_snowtrooper_helmet = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_snowtrooper_helmet.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_snowtrooper_helmet", painting_snowtrooper_helmet)
