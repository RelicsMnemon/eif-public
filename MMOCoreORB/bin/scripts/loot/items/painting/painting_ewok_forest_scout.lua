painting_ewok_forest_scout = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_ewok_forest_scout.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_ewok_forest_scout", painting_ewok_forest_scout)
