painting_snowtrooper_helmet_wide = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_snowtrooper_helmet_wide.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_snowtrooper_helmet_wide", painting_snowtrooper_helmet_wide)
