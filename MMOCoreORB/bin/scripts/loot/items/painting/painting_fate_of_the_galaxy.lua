painting_fate_of_the_galaxy = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_fate_of_the_galaxy.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_fate_of_the_galaxy", painting_fate_of_the_galaxy)
