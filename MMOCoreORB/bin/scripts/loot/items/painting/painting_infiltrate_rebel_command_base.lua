painting_infiltrate_rebel_command_base = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_infiltrate_rebel_command_base.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_infiltrate_rebel_command_base", painting_infiltrate_rebel_command_base)
