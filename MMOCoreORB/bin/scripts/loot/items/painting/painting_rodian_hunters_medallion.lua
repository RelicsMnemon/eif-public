painting_rodian_hunters_medallion = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_rodian_hunters_medallion.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_rodian_hunters_medallion", painting_rodian_hunters_medallion)
