painting_imperial_celebration_organizer = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_imperial_celebration_organizer.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_imperial_celebration_organizer", painting_imperial_celebration_organizer)
