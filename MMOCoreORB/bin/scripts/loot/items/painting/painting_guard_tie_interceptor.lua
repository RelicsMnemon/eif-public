painting_guard_tie_interceptor = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_guard_tie_interceptor.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_guard_tie_interceptor", painting_guard_tie_interceptor)
