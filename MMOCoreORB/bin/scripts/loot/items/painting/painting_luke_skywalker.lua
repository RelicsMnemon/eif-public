painting_luke_skywalker = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_luke_skywalker.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_luke_skywalker", painting_luke_skywalker)
