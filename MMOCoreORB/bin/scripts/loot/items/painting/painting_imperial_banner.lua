painting_imperial_banner = {
        minimumLevel = 0,
        maximumLevel = 0,
        customObjectName = "",
        directObjectTemplate = "object/tangible/painting/painting_imperial_banner.iff",
        craftingValues = {
        },
        customizationStringNames = {},
        customizationValues = {}
}

addLootItemTemplate("painting_imperial_banner", painting_imperial_banner)
