--EiF generated

sun_guard_armor_schematics = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "armor_sun_guard_belt_schematic", weight = 909091},
		{itemTemplate = "armor_sun_guard_bicep_l_schematic", weight = 909091},
		{itemTemplate = "armor_sun_guard_bicep_r_schematic", weight = 909091},
		{itemTemplate = "armor_sun_guard_boots_schematic", weight = 909091},
		{itemTemplate = "armor_sun_guard_bracer_l_schematic", weight = 909091},
		{itemTemplate = "armor_sun_guard_bracer_r_schematic", weight = 909091},
		{itemTemplate = "armor_sun_guard_cape_schematic", weight = 909091},
		{itemTemplate = "armor_sun_guard_chest_plate_schematic", weight = 909091},
		{itemTemplate = "armor_sun_guard_gloves_schematic", weight = 909091},
		{itemTemplate = "armor_sun_guard_helmet_schematic", weight = 909091},
		{itemTemplate = "armor_sun_guard_leggings_schematic", weight = 909090}
	}
}

addLootGroupTemplate("sun_guard_armor_schematics", sun_guard_armor_schematics)
