--Copyright (C) 2017 <SWG EiF>


--This File is part of SWG: Empire in Flames. It is not designed to be redistributed or used in other SWGEMU based emulators or any other emulators.
--If you wish to use this file, please contact the Empire in Flames team for permission to do so.
--This software is distributed without any warranty; without an implied warranty of merchantability or fitness for a particular purpose.


object_mobile_vehicle_single_pod_airspeeder = object_mobile_vehicle_shared_single_pod_airspeeder:new {
	templateType = VEHICLE,
	decayRate = 25, -- Damage tick per decay cycle
	decayCycle = 600 -- Time in seconds per cycle
}

ObjectTemplates:addTemplate(object_mobile_vehicle_single_pod_airspeeder, "object/mobile/vehicle/single_pod_airspeeder.iff")
